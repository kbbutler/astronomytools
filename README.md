# Astrophotography Target Identification Utility



## Credits
All credit for the algorithms in this tool go to [James Lamb](https://www.youtube.com/user/Aero19612). While I (Kyle Butler) ported this code to python for broader consumption, James did all of the original leg work in Scilab.

## Purpose
The purpose of this tool is twofold:

 - To help astrophotographers prioritize their imaging to-do list by identifying the ideal time frame for imaging their targets, based on their location and horizon. 
 - To help astrophotographers *discover* new targets which are visible from their location, and provide them with ideal imaging time information for those targets.

While it is possible to get this sort of information from other tools such as Stellarium, it is difficult to obtain this information in a bulk form. Additionally, while other image planning tools may give you a rough idea of the ideal season to image a target, this tool will tell you the exact peak imaging date, helping you maximize your ideal imaging time.

## Prerequisites
 - Python 3.6 or later is recommended (https://www.python.org/downloads)
 
## Usage instructions

### Setup
 - Make sure python 3.6 or newer is installed.
 - Install the necessary python packages: `pip3 install -r requirements.txt`
 - Clone or download the code repository
 - In the /src directory, modify config.json with your particular information
     - "Lat" : latitude of observing location in decimal degrees format; positive values are north latitude, negative values are south latitude.
     - "Lon" : longitude of observing location in decimal degrees format; positive values are eastern longitudes, negative values are western longitudes
     - "Horizon_x" : x coordinates of horizon map (0 to 360 degrees); 0 = N, 90 = E, 180 = S, 270 = W.
     - "Horizon_y" : y coordinates of horizon map (0 to 90 degrees); 0 = lowest horizon, 90 = zenith
     - "magLim" : limiting magnitude of results (do not show targets with magnitudes above this limit)
     - "sizLimMin" : Do not show targets smaller than this; units are in arc minutes
     - "sizLimMax" : Do not show targets larger than this; units are in arc minutes
     - "hrLim" : minimum time in hours that a target must be imageable to be included
     - "altLim" : minimum altitude in degrees for a target to be considered imageable
     - "altMin" : minimum altitude in degrees for a target to be considered visible

Tip: Horizon information can be generated using a tool like [Dioptra](https://play.google.com/store/apps/details?id=com.glidelinesystems.dioptra&hl=en_CA&gl=US)

### Running the tool

 - Run the tool using one of the supported modes
	 - **Single target mode**:  `./astrolookup.py --target m45`
	 - **File mode**: `./astrolookup.py --file example_target_list.csv`
		Note: If using file mode, input file should be a file with one target per line. An example list called example_target_list.csv is provided in the repo for your reference.
- If you wish to output the results to a file instead of the command line, use the optional **--output** option.
	- Example: ./astrolookup.py --file example_target_list.csv **--output results.csv**
- If you wish to bypass the magnitude, size, and altitude limits defined in config.json, you can use the **--nolimit** flag.
