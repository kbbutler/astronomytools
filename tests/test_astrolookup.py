import pytest
import unittest
import astrolookup

assertions = unittest.TestCase('__init__')

#IC0010(0.00): Max Score: 0.86, Max Observation Time: 11.7 Hours, Maximum Altitude: 73.8		10-20
def test_ic0010():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0010")
    assert max_score == pytest.approx(0.86, abs=0.05)
    assert max_hours_visible == pytest.approx(11.7, abs=0.7)
    assert max_altitude == pytest.approx(73.8, abs=1)
    assert mon == 10
    assert day == pytest.approx(20, abs=7)

#IC0034(0.00): Max Score: 0.37, Max Observation Time:  6.6 Hours, Maximum Altitude: 55.8		09-14
def test_ic0034():
    result = astrolookup.get_target("ic0034")
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0034")
    assert max_score == pytest.approx(0.37, abs=0.05)
    assert max_hours_visible == pytest.approx(6.6, abs=0.7)
    assert max_altitude == pytest.approx(55.8, abs=1)
    assert mon == 9
    assert day == pytest.approx(14, abs=7)

#IC0059(0.00): Max Score: 0.87, Max Observation Time: 12.2 Hours, Maximum Altitude: 72.0		11-07
def test_ic0059():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0059")
    assert max_score == pytest.approx(0.87, abs=0.05)
    assert max_hours_visible == pytest.approx(12.2, abs=0.7)
    assert max_altitude == pytest.approx(72.0, abs=1)
    assert mon == 11
    assert day == pytest.approx(7, abs=7)

#IC0063(0.00): Max Score: 0.87, Max Observation Time: 12.2 Hours, Maximum Altitude: 72.2		11-07
def test_ic0063():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0063")
    assert max_score == pytest.approx(0.87, abs=0.05)
    assert max_hours_visible == pytest.approx(12.2, abs=0.7)
    assert max_altitude == pytest.approx(72.2, abs=1)
    assert mon == 11
    assert day == pytest.approx(7, abs=7)

#IC0065(0.00): Max Score: 0.99, Max Observation Time: 11.7 Hours, Maximum Altitude: 85.1		10-20
def test_ic0065():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0065")
    assert max_score == pytest.approx(0.99, abs=0.05)
    assert max_hours_visible == pytest.approx(11.7, abs=0.7)
    assert max_altitude == pytest.approx(85.1, abs=1)
    assert mon == 10
    assert day == pytest.approx(20, abs=7)

#IC0155(0.00): Max Score: 0.88, Max Observation Time: 12.2 Hours, Maximum Altitude: 72.5		11-07
def test_ic0155():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0155")
    assert max_score == pytest.approx(0.88, abs=0.05)
    assert max_hours_visible == pytest.approx(12.2, abs=0.7)
    assert max_altitude == pytest.approx(73.0, abs=1)
    assert mon == 11
    assert day == pytest.approx(7, abs=7)

#IC0239(0.00): Max Score: 0.81, Max Observation Time:  9.7 Hours, Maximum Altitude: 85.0		11-01
def test_ic0239():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0239")
    assert max_score == pytest.approx(0.81, abs=0.05)
    assert max_hours_visible == pytest.approx(9.7, abs=0.7)
    assert max_altitude == pytest.approx(85.0, abs=1)
    assert (mon == 10 and day >= 25) or (mon == 11 and day <= 6)

#IC0284(0.00): Max Score: 0.93, Max Observation Time: 10.7 Hours, Maximum Altitude: 87.4		11-07
def test_ic0284():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0284")
    assert max_score == pytest.approx(0.93, abs=0.05)
    assert max_hours_visible == pytest.approx(10.7, abs=0.7)
    assert max_altitude == pytest.approx(87.4, abs=1)
    assert mon == 11
    assert day == pytest.approx(7, abs=7)

#IC0335(0.01): Does not satisfy observation limits: Hours:  0.0, Maximum Altitude:  0.0
def test_ic0335():
    result = astrolookup.get_target("ic0335")
    assert result is None

#IC0341(0.01): Max Score: 0.48, Max Observation Time:  7.1 Hours, Maximum Altitude: 68.7		10-26
def test_ic0341():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("ic0341")
    assert max_score == pytest.approx(0.48, abs=0.05)
    assert max_hours_visible == pytest.approx(7.1, abs=0.7)
    assert max_altitude == pytest.approx(68.7, abs=1)
    assert (mon == 10 and day >= 19) or (mon == 11 and day <= 2)

def test_m63():
    name, type, subtype, max_score, max_hours_visible, max_altitude, mon, day = astrolookup.get_target("m63")
    assert max_score == pytest.approx(0.77, abs=0.05)
    assert max_hours_visible == pytest.approx(8.66, abs=0.7)
    assert max_altitude == pytest.approx(87.5, abs=1)
    assert (mon == 3 and day >= 26) or (mon == 4 and day <= 5)




