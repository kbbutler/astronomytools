numpy>=1.18.1
oauth>=1.0.1
pytest>=6.1.1
requests>=2.22.0
simplejson>=3.13.2
