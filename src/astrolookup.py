#!/usr/bin/env python3
import sys
import requests
import math
import argparse
import json
import numpy as np

args = None

### User params
Lat, Lon, Horizon_x, Horizon_y, magLim, sizLimMin, sizLimMax, hrLim, altLim, altMin = \
    None, None, None, None, None, None, None, None, None, None

#### Program constants
nDiv = 24 * 2  # increase number for smoother plot ((this is number of divisions in 24 hour period)
Nyr = 365  # days in a year
Nyr_increment = 5 # days to skip by (set to 1 for count every day)
Rt = 1
Rs = 1
Re = 1
psi = -23.4 * math.pi / 180  # inclination rel to ecliptic
Tday = 23 + 56 / 60 + 4.0916 / 60 / 60  # Rotation period in hours
Om = 2 * math.pi / Tday  # Rotation rate (rad/hour)
Trev = 365.25635 * Tday  # Revolution around Sun in hours
w = 2 * math.pi / Trev  # Orbital rate (rad/hour)
R01 = None
R23 = None
t = 0
dt = Tday / (nDiv - 1)

def load_user_config():
    global Lat, Lon, Horizon_x, Horizon_y, magLim, sizLimMin, sizLimMax, hrLim, altLim, altMin, R01, R23

    with open('../config.json') as fp:
        config = json.load(fp)

    Lat = float(config["Lat"]) * math.pi / 180
    Lon = float(config["Lon"]) * math.pi / 180

    Horizon_x = []
    for xcoord in config["Horizon_x"]:
        Horizon_x.append(xcoord)

    Horizon_y = []
    for ycoord in config["Horizon_y"]:
        Horizon_y.append(ycoord)

    if len(Horizon_x) != len (Horizon_y):
        assert("Horizon provided is of inequal length (x = {}, y = {}" .format(len(Horizon_x), len(Horizon_y)))

    magLim = config["magLim"]
    sizLimMin = config["sizLimMin"]
    sizLimMax = config["sizLimMax"]
    hrLim = config["hrLim"]
    altLim = config["altLim"]
    altMin = config["altMin"]

    R01 = np.matrix([[math.cos(psi), 0.0, -1 * math.sin(psi)],
                     [0.0, 1.0, 0.0],
                     [math.sin(psi), 0.0, math.cos(psi)]])

    R23 = np.matrix([[1.0, 0.0, 0.0],
                     [0.0, math.cos(Lat), math.sin(Lat)],
                     [0.0, -1 * math.sin(Lat), math.cos(Lat)]])

def get_object_details(object_name):
    lookup="http://simbad.u-strasbg.fr/simbad/sim-tap/sync?request=doQuery&lang=adql&format=JSON&query=" \
           "SELECT id, otype_txt, morph_type, ra, dec, galdim_majaxis, galdim_minaxis, flux " \
           "FROM basic JOIN ident ON ident.oidref = oid LEFT OUTER JOIN flux ON flux.oidref = oid AND flux.filter = 'V' " \
           "WHERE id = '{}';".format(object_name)
    r = requests.get(lookup).json()
    object = dict()

    if len(r["data"]) > 1:
        print("More than one result was returned for {}".format(object_name))
        return None
    elif len(r["data"]) == 0:
        print("No results were returned for {} when querying the server.".format(object_name))
        return None
    else:
        for item in r["data"]:
            if item[0]:
                object["name"] = item[0]
            else:
                return None

            if item[1]:
                object["type"] = item[1]
            else:
                object["type"] = ""

            if item[2]:
                object["subtype"] = item[2]
            else:
                object["subtype"] = ""

            if item[3]:
                object["ra"] = float(item[3]) / 15.0
            else:
                return None

            if item[4]:
                object["dec"] = item[4]
            else:
                return None

            if item[5] and item[6]:
                object["size"] = max(float(item[5]), float(item[6]))
            else:
                object["size"] = -1

            if item[7]:
                object["mag"] = item[7]
            else:
                object["mag"] = 0.0

        return object

def create_damo_lookup(Nyr):
    dayMonth = [11, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31, 28, 20]
    k = 0
    damo = [[0 for i in range(3)] for j in range(Nyr)]
    for i in range(0, len(dayMonth)):
        for j in range(0, dayMonth[i]):
            if i == 0:
                day = j + 21
            else:
                day = j + 1
            mon = i + 3
            if mon > 12:
                mon = mon - 12
            damo[k] = [mon, day, day / 32]
            k = k + 1
    return damo


def get_target(name):
    load_user_config()

    damo = create_damo_lookup(Nyr)
    ########################################
    object = get_object_details(name)
    if object is None:
        return None
    TargetName = object["name"]
    ObjectType = object["type"]
    ObjectMorp = object["subtype"]
    RAh        = float(object["ra"])
    DECd       = float(object["dec"])
    Siz        = float(object["size"])
    mag        = float(object["mag"])
    RAd        = (RAh)*2*math.pi/Tday
    history = [[0 for i in range(7)] for j in range(Nyr)]
########################################
    #rTS =  R01'*Rt*[-sin(a)*cos(d); cos(a)*cos(d); sin(d)];
    rTS =  np.matrix.transpose(R01)*Rt*np.matrix.transpose(np.matrix([-1*math.sin(RAd)*math.cos(DECd*math.pi/180), math.cos(RAd)*math.cos(DECd*math.pi/180), math.sin(DECd*math.pi/180)]))
    rPE =  np.matrix('0.0; 1.0; 0.0')

    target_meets_criteria = (args and args.nolimit) or ((mag < magLim) and ((Siz < 0) or (Siz <= sizLimMax and Siz >= sizLimMin)))
    if target_meets_criteria:
        inView = np.zeros((Nyr * nDiv, 3))
        for j in range(0, Nyr, Nyr_increment):
            t0 = j * Tday
            hVis = 0
            aMax = 0
            aMin = 100
            for i in range(0, nDiv):
                t = i * dt + t0
                R12 = np.matrix([[math.cos(Om * t + Lon), math.sin(Om * t + Lon), 0.0],
                                 [-math.sin(Om * t + Lon), math.cos(Om * t + Lon), 0.0],
                                 [0.0, 0.0, 1.0]])
                rES = Rs * np.matrix([[math.sin(w * t), -math.cos(w * t), 0.0]])
                # rTP =  R23*R12*R01*(rTS-rES*1.56e-6*0) //- rPE
                rTP = R23 * R12 * R01 * (rTS - rES * 1.56e-6 * 0)  # - rPE"
                north = rTP[2, 0]
                west = rTP[0, 0]
                up = rTP[1, 0]
                rSP = -R23 * R12 * R01 * np.transpose(rES)
                altSun = np.arctan2(rSP[1, 0], math.sqrt((rSP[0, 0] ** 2 + rSP[2, 0] ** 2))) * 180 / math.pi
                az = -np.arctan2(west, north) * 180 / math.pi
                if az < 0.0:
                    az = 360 + az

                if az > 360:
                    az = az - 360

                altHorz = np.interp(az, Horizon_x, Horizon_y)
                alt = np.arctan2(up, math.sqrt(north ** 2 + west ** 2)) * 180 / math.pi
                # inView(i + (j-1)*nDiv,1) = j+(i-1)/nDiv-1
                inView[i + j * nDiv][0] = (j + 1) + i / nDiv - 1
                if altSun < -12:
                    # It is dark outside
                    # inView(i + (j-1)*nDiv,2) = alt
                    inView[i + j * nDiv][1] = alt
                    if alt > altHorz and alt > altMin:
                        # Target is above the roof line and minimum altitude
                        hVis = hVis + dt
                        aMax = max(alt, aMax)
                        aMin = min(alt, aMin)
                        # inView(i + (j-1)*nDiv,3) = alt
                        inView[i + j * nDiv][2] = alt
            if aMin > 90:
                aMin_value = 0
            else:
                aMin_value = aMin
            if aMax > altLim and hVis > hrLim:
                valid = 1
            else:
                valid = 0

            history[j] = [j, hVis, aMax, aMin_value, 0.0, valid, (hVis / 11) * (aMax / 90)]
    else:
        print("{}: Does not satisfy magnitude or size limits (Mag limit: {}, Target Mmag: {}, Size Limit: {}-{}, Target Size: {}".format(TargetName, magLim, mag, sizLimMin, sizLimMax, Siz))
        return None

    #=========================================================================469.232.5551
    #  Find First Day When Obs Time > hrLim and maximum altitude > aMax
    #=========================================================================
    valid_target = 0
    max_hours_visible = 0.0
    max_altitude = 0.0
    valid_count = 0

    for r_history in history:
        max_altitude = max(max_altitude, r_history[2])
        max_hours_visible = max(max_hours_visible, r_history[3])
        if r_history[5] == 1:
            valid_target = 1
            valid_count += 1

    if not valid_target:
        print("{}: Does not satisfy observation limits: Maximum Altitude: {}, Maximum Hours Visible: {}".format(TargetName,max_altitude,max_hours_visible))
        return None
    else:
        if valid_count == 365:
            jj = 0  # visible all of the time
        else:
            jj = 0 # visible some of the time
            if history[0][5] == 1:  # not visible at Equinox
                while history[jj][5] == 0:
                    jj = jj+1
                    jj = min(max(0,jj),364)
                    # jj = Day target is first visible
            else:
                jj = Nyr-1       # visible at Equinox, start at end of year
                while history[jj][5] == 1:
                    jj = jj-1
                    jj = min(max(0,jj),364)

                jj = jj + 1   # jj = Day target is first visible
                jj = min(max(0,jj),364)

        jj = min(max(0,jj),364)
        #hMat = sortMat([(damo(:,1)+damo(:,3)),history(:,4),history(:,3),history(:,2), history(:,7)],1,'i')
        sMax = 0
        for i in range(0, Nyr, Nyr_increment):
            sMax = max(0.98 * history[i][6], sMax)
        kk = 0

        if history[1][6] < sMax:  # not visible at Equinox
            while history[kk][6] < sMax:
                kk = kk+1
                kk = min(max(0,kk),364)
                        # jj = Day target is first visible
        else:
            kk = Nyr-1       # visible at Equinox, start at end of year
            while kk >= 0 and history[kk][6] >= sMax:
                kk = kk-1

            kk = kk + 1   # jj = Day target is first visible
            kk = min(max(0,kk),364)

            if kk < 0:
                print(
                    "{}: Does not satisfy observation limits: Hours: {}, Maximum Altitude: {}".format(TargetName,
                                                                                                         max_hours_visible,
                                                                                                         max_altitude))
                return None
        kk = min(max(0,kk),364)

        max_score = 0.0
        max_hours_visible = 0.0
        max_altitude = 0.0
        for r_history in history:
            max_score = max(max_score, r_history[6])
            max_hours_visible = max(max_hours_visible, r_history[1])
            max_altitude = max(max_altitude, r_history[2])

        # jj = day target is first visible, kk = date of max score
        return TargetName, ObjectType, ObjectMorp, max_score, max_hours_visible, max_altitude, damo[kk][0],damo[kk][1]

def main(args):
    if args.output:
        with open (*args.output, "w+") as fo:
            fo.write("Target Name, Type, Sub-type, Max Score, Max Observation Time, Maximum Altitude, Ideal Imaging Date\n")

    if args.file:
        with open(*args.file, "r") as fc:
            for name in fc:
                result = get_target(name)
                if(result):
                    if(args.output):
                        with open(*args.output, "a+") as fo:
                            fo.write("{}, {}, {}, {:0.2f}, {:0.2f}, {:0.1f}, {}-{}\n".format(*result))
                    else:
                        print("{}, ({} {}): Max Score: {:0.2f}, Max Observation Time: {:0.2f} Hours, Maximum Altitude:{:0.1f}, Max Score Date: {}-{}".format(*result))

    elif args.target:
        result = get_target(*args.target)
        if (result):
            if (args.output):
                with open(*args.output, "a+") as fo:
                    fo.write("{}, {}, {}, {:0.2f}, {:0.2f}, {:0.1f}, {}-{}\n".format(*result))
            else:
                print("{}, ({} {}): Max Score: {:0.2f}, Max Observation Time: {:0.2f} Hours, Maximum Altitude:{:0.1f}, Max Score Date: {}-{}".format(*result))

def parse_args():
    parser = argparse.ArgumentParser(description='Astronomy ideal target information.')
    parser.add_argument('--file', nargs=1,
                        help='file with target list, one per line')
    parser.add_argument('--output', nargs=1,
                        help='file to write results to in csv format')
    parser.add_argument('--nolimit', action='store_true',
                        help='Do not limit targets by magnitude or size.')
    parser.add_argument('--target', nargs=1,
                        help='target name')

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    sys.exit(main(args))